<?php

/**
 * @file
 * Google Tag Manager integration.
 */

$plugin = array(
  'title' => 'Google Tag Manager',
  'description' => 'Handle cookies related to Google Tag Manager',
  'module_dependencies' => array('google_tag'),
  'blacklist' => array('/google_tag/'),
  'opt_in_category' => 'analytics',
);
