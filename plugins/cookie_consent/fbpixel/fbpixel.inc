<?php

/**
 * @file
 * Google Adsense integration.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'title' => 'Facebook Pixel',
  'description' => 'Handle cookies related to the Google Adsense service',
  'blacklist' => array('/facebook/'),
  'opt_in_category' => 'marketing',
);
